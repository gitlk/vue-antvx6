import Vue from 'vue'
import Vuex from 'vuex'
 
Vue.use(Vuex)
 
export default new Vuex.Store({
  state: {
    videoUrl:"邓杰"
  },
  mutations: {
     getVideoUrl(state,videoUrl){
        state.videoUrl = videoUrl
     }
  },
  actions: {
 
  }
});